import { render, screen } from '@testing-library/react'
import App from './App'
import ReactDOM from 'react-dom';
import MessageDTO from './Common/MessageDTO.js'

test('should render Send button text', () => {
  render(<App />)
  const linkElement = screen.getByText(/Send/i)
  expect(linkElement).toBeInTheDocument()
})

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<App />, div)
})

it('creates serialized MessageDTO', () => {
  const MessageDTOToCompare = {
      messageBody: 'test text',
      id: 1,
      senderAvatar: './avatar.jpg',
      createdDate: new Date().toLocaleString("ua")
  }

  expect(
      MessageDTO.serialize('test text', 1, './avatar.jpg').toString()
  ).toEqual(MessageDTOToCompare.toString())
})


