class MessageDTO {
    /**
     *
     * @param body
     * @param userId
     * @param avatar
     * @return {{id: *, createdDate: *, messageBody: *, senderAvatar: *}}
     */
    static serialize(body, userId, avatar) {
        return {
            messageBody: body,
            id: userId,
            senderAvatar: avatar,
            createdDate: new Date().toLocaleString("ua")
        }
    }
}

export default MessageDTO
