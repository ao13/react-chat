import userImg from '../assets/images/userPhoto.jpg'
import interlocutorImg from '../assets/images/frozen2.jpg'

let user = {
    id: 1,
    name: 'John',
    lastName: 'Doe',
    avatar: userImg
}

let interlocutor = {
    id: 5,
    name: 'Test',
    lastName: 'Testovich',
    avatar: interlocutorImg
}

const UserService = {
    /**
     *
     * @return {{lastName: string, name: string, id: number, avatar: *}}
     */
    getUser: () => user,

    /**
     *
     * @return {{lastName: string, name: string, id: number, avatar: *}}
     */
    getInterlocutor: () => interlocutor
}

export default UserService
