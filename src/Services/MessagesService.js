import frozen from '../assets/images/frozen2.jpg'
import { BehaviorSubject } from 'rxjs'

let initialMessages = [
    {
        messageBody: 'Access to device capabilities like camera, location, notifications, sensors, haptics, and much more, all with universal APIs.',
        id: 5,
        senderAvatar: frozen,
        createdDate: new Date(2020, 10, 29).toLocaleString('ua')
    },
    {
        messageBody: 'Access to device capabilities like camera, location, notifications, sensors,',
        id: 5,
        senderAvatar: frozen,
        createdDate: new Date(2019, 10, 29).toLocaleString('ua')
    }
]

const messagesStack = new BehaviorSubject(initialMessages)

const MessagesService = {
    /**
     *
     * @param currentMessagesStack
     * @param newMessage
     */
    addMessage: (currentMessagesStack, newMessage) => {
        messagesStack.next([...currentMessagesStack, newMessage])
    },

    /**
     *
     * @return {Observable<*[]>}
     */
    getAllMessages: () => messagesStack.asObservable()
}

export default MessagesService
