import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledInput = styled.input`
     color: ${props => props.color ? props.color : 'black'};
     padding: 8px 12px;
     font-size: 1.2rem;
     border: 1px solid ${props => props.borderColor ? props.borderColor : 'gray'};
     &:focus{
         outline: none;
     }
`

const Input = React.forwardRef(({
    value = '',
    onChange = (f) => f,
    borderColor = '',
    color = '',
    ...props
}, ref) =>
    <StyledInput
        value={value}
        onChange={onChange}
        borderColor={borderColor}
        color={color}
        {...props}
        ref={ref}
    />
)

Input.propTypes = {
    value: PropTypes.string.isRequired,
    borderColor: PropTypes.string,
    color: PropTypes.string,
    onChange: PropTypes.func.isRequired
}

export default Input
