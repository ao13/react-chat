import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

const Button = styled.button`
    display: block;
    cursor: pointer;
    border: 1px solid transparent;
    border-radius: 4px;
    background-color: rgb(0 132 255);
    font-size: 1.2rem;
    color: white;
    padding: 8px 12px;
    min-height: fit-content;
    
    &:hover {
        background-color: #4096e3;
    }
    ${(props) =>
            props.disabled &&
            css`
                opacity: 0.4;
                background-color: gray;
                cursor: not-allowed;
                &:hover {
                    background-color: gray;
            `};
`

Button.propTypes = {
    disabled: PropTypes.bool.isRequired
}

export default Button
