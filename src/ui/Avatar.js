import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledImg = styled.img`
    border-radius: 50%;
    border: 1px solid grey;
    width: ${(props) => {
    switch (props.size) {
        case 'sm':
            return '20px'
        case 'md':
            return '50px'
        case 'lg':
            return '60px'
        default:
            return '20px'
    }
}};
    height: ${(props) => {
    switch (props.size) {
        case 'sm':
            return '20px'
        case 'md':
            return '50px'
        case 'lg':
            return '60px'
        default:
            return '20px'
    }
}};
`

const Avatar = ({ src = '', size = 'sm' }) =>
    <StyledImg src={src} size={size} />

Avatar.propTypes = {
    src: PropTypes.string.isRequired,
    size: PropTypes.oneOf(['sm', 'md', 'lg']).isRequired
}

export default Avatar
