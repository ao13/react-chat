import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'
import Input from '../ui/Input'
import Button from '../ui/Button'
import PropTypes from 'prop-types'

const StyledInput = styled(Input)`
    max-width: 100%;
    width: 100%;
    min-width: 150px;
`

const StyledForm = styled.form`
    display: flex;
    justify-content: space-between;
    width: 100%;
`

const Sender = ({ onMessageSendHandler = f => f }) => {
    const inputEl = useRef(null)
    const [message, setMessage] = useState('');
    const [disabled, setDisabled] = useState(true);

    useEffect(() => {
        message ? setDisabled(false) : setDisabled(true)
    }, [message])

    const onMessageSend = (event) => {
        inputEl.current.focus()
        event.preventDefault()
        onMessageSendHandler(message)
        setMessage('')
        setDisabled(true)
    }

    const inputHandler = (event) => {
        setMessage(event.target.value)
        setDisabled(false)
    }

    return (
        <StyledForm>
            <StyledInput
                placeholder={'Type a message'}
                value={message}
                onChange={inputHandler}
                borderColor={'transparent'}
                ref={inputEl}
            />
            <Button
                onClick={onMessageSend}
                disabled={disabled}
            >
                Send
            </Button>
        </StyledForm>
    )
}

Sender.propTypes = {
    onMessageSendHandler: PropTypes.func.isRequired
}

export default Sender
