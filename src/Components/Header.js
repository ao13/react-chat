import React from 'react'
import styled from 'styled-components'
import Avatar from '../ui/Avatar'
import PropTypes from 'prop-types'

const HeaderContainer = styled.div`
   display: flex;
   justify-content: center;
   background-color: rgb(0 132 255);
   padding: 0.8rem 0;
   width: 100%;
   max-height: min-content;
   box-shadow: 0 2px 4px 0px rgba(0, 0, 0, 1);
`

const ContentContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    max-width: min-content;
`

const StyledText = styled.span`
    font-size: 1.5rem;
    color: white;
    margin-left: 1rem;
    width: max-content;
`

const Header = ({ user }) =>
    <HeaderContainer>
        <ContentContainer>
            <Avatar src={user.avatar} size={'md'} />
            <StyledText>
                {user.name + ' ' + user.lastName}
            </StyledText>
        </ContentContainer>
    </HeaderContainer>

Header.propTypes = {
    user: PropTypes.object.isRequired
}

export default Header
