import React from 'react'
import styled, { css } from 'styled-components'
import Avatar from "../ui/Avatar"
import PropTypes from 'prop-types'

const MessageContainer = styled.div`
    display: flex;
    position: relative;
    width: 50%;
    max-width: 250px;
    height: auto;
    ${(props) =>
        props.position === 'Right' &&
        css`
            flex-direction: row-reverse;     
        `};
`

const AvatarContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    height: 100%;
    width: 50px;
`

const ContentContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    width: auto;
    margin-left: 5px;
    margin-right: 5px;
    padding: 0.3rem 0.7rem;
    background-color: rgb(228 230 235);
    border-radius: 4px;
    ${(props) =>
    props.position === 'Right' &&
    css`
         align-items: flex-end;     
         background-color: rgb(0 132 255);
         color: white;
    `};
`

const SubText = styled.span`
    color: inherit;
    font-size: 12px;
`

const Message = ({
     position = 'Left',
     message = {},
     showAvatar = false
}) => {
    return (
        <MessageContainer position={position}>
            <AvatarContainer >
                {showAvatar && <Avatar size={'sm'} src={message.senderAvatar} />}
            </AvatarContainer>
            <ContentContainer position={position}>
                <p>{message.messageBody}</p>
                <SubText>{message.createdDate}</SubText>
            </ContentContainer>
        </MessageContainer>
    )
}

Message.propTypes = {
    position: PropTypes.string,
    message: PropTypes.shape({
        senderAvatar: PropTypes.string,
        messageBody: PropTypes.string,
        createdDate: PropTypes.string
    }).isRequired,
    showAvatar: PropTypes.bool
}

export default Message
