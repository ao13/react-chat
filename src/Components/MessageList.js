import React from 'react'
import styled from 'styled-components'
import Message from './Message'
import PropTypes from 'prop-types'

const ListContainer = styled.ul`
    list-style-type: none;
    background-color: #f5f5f5c7;
    width: 100%;
    min-height: 100%;
    height: auto;
    margin: 0;
    padding: 2rem 0;
`

const ListItem = styled.li`
    display: flex;
    padding: 0.3rem 0.7rem;
    justify-content: ${(props) => 
        props.position === 'Right' ? 'flex-end' : props.position === 'Left' ? 'flex-start' : 'space-between'
};
`

const MessageList = ({
    messages = [],
    user = {},
    interlocutor = {}
}) => {
    const ids = messages.map(m => m.id)
    const senderLastMessageIndex = ids.lastIndexOf(user.id)
    const interlocutorMessageIndex = ids.lastIndexOf(interlocutor.id)

    return (
        <ListContainer>
            {messages.map((message, index) => {
                const messagePosition = message.id === user.id ? 'Right' : 'Left'
                return (
                    <ListItem key={index} position={messagePosition}>
                        <Message
                            position={messagePosition}
                            showAvatar={index === interlocutorMessageIndex || index === senderLastMessageIndex}
                            message={message}
                        />
                    </ListItem>
                )
            })}
        </ListContainer>
    )
}

MessageList.propTypes = {
    messages: PropTypes.array.isRequired,
    user: PropTypes.object.isRequired,
    interlocutor: PropTypes.object.isRequired
}

export default MessageList
