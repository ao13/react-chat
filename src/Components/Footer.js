import React from 'react'
import styled from 'styled-components'
import Sender from './Sender'
import PropTypes from 'prop-types'

const FooterContainer = styled.div`
    display: flex;
    padding: 0.8rem 2rem;
    border-top: 1px solid lightgray;
`

const Footer = ({ onMessageSendHandler = f => f }) =>
    <FooterContainer>
        <Sender onMessageSendHandler={onMessageSendHandler} />
    </FooterContainer>

Footer.propTypes = {
    onMessageSendHandler: PropTypes.func.isRequired
}

export default Footer
