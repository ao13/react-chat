import './App.css'
import React from 'react'
import ChatRoomScreen from "./Screens/ChatRoomScreen"
import UserService from './Services/UserService'

const App = () => {
  return (
      <ChatRoomScreen
          user={UserService.getUser()}
          interlocutor={UserService.getInterlocutor()}
      />
  )
}

export default App
