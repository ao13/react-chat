import React, {useState, useEffect} from 'react'
import styled from 'styled-components'
import Header from '../Components/Header'
import Footer from '../Components/Footer'
import MessageList from "../Components/MessageList"
import MessagesService from "../Services/MessagesService"
import MessageDTO from "../Common/MessageDTO"

const PageContainer = styled.div`
    display: flex;
    width: 100%;
    height: 100vh;
    flex-direction: column;
    justify-content: space-between;
`
const ContentContainer = styled.div`
    width: 100%;
    height: 100%;
    overflow-y: scroll;
`

const ChatRoomScreen = ({ user, interlocutor }) => {
    const [messagesList, setMessagesList] = useState([])

    const addmessageToStack = (messageText) => {
        MessagesService.addMessage(
            messagesList,
            MessageDTO.serialize(messageText, user.id, user.avatar)
        )
    }

    useEffect(() => {
       const messageSubscription = MessagesService.getAllMessages().subscribe(messages => {
           setMessagesList(messages)
       })

        return () => {
            messageSubscription.unsubscribe()
        }
    }, [])

    return (
        <PageContainer>
            <Header user={interlocutor} />
            <ContentContainer>
                <MessageList
                    messages={messagesList}
                    user={user}
                    interlocutor={interlocutor}
                />
            </ContentContainer>
            <Footer
                onMessageSendHandler={addmessageToStack}
                userInfo={user}
            />
        </PageContainer>
    )
}

export default ChatRoomScreen
